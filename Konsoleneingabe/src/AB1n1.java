import java.util.Scanner;

public class AB1n1 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
	    double zahl1 = myScanner.nextDouble();  
	     
	    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");  
	    
	    double zahl2 = myScanner.nextDouble();
	     
	    double addition = zahl1 + zahl2;  
	    double subtraction = zahl1 - zahl2;  
	    double multiplication = zahl1 * zahl2;  
	    double division = zahl1 / zahl2;  
	     
	    System.out.print("\n\n\nErgebnis der Addition lautet: ");
	    System.out.print(zahl1 + " + " + zahl2 + " = " + addition);
	    
	    System.out.print("\nErgebnis der Subtraktion lautet: ");
	    System.out.print(zahl1 + " - " + zahl2 + " = " + subtraction);
	    
	    System.out.print("\nErgebnis der Multiplikation lautet: ");
	    System.out.print(zahl1 + " * " + zahl2 + " = " + multiplication);
	    
	    System.out.print("\nErgebnis der Division lautet: ");
	    System.out.print(zahl1 + " / " + zahl2 + " = " + division);
	    
	    myScanner.close();
	}

}
