import java.util.Scanner;

public class AB1n2 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Wie hei�t du? ");
		String name = myScanner.next();
		
		System.out.print("\nWie alt bist du? ");
		byte age = myScanner.nextByte();
		
		System.out.print("Du hei�t " + name + " und bist " + age + " Jahre alt.");
		
		myScanner.close();
	}
}
